# wbtech

python3.6.7

## start project

### 1. install requirements

`pip install -r requirements`

### 2. run development smpt server

`python -m smtpd -n -c DebuggingServer localhost:1025`

### 3. run development server

`python manage.py runserver`

## endpoints

all endpoints allow OPTION method
----------------------------------------------------------------------

# registration

POST 
/sign_up/

input:

username
email
password1
password2

output:

```
{
    "key": "e7101c7defc139f7a0c4dbe8c664aaa3cfb010d3"
}
```
----------------------------------------------------------------------

GET PUT
/user/
Your profile

'PUT'
input:
username
email
first_name
last_name

output:
```
{
    "pk": 2,
    "username": "nosipov",
    "email": "nosipov@gmail.com",
    "first_name": "",
    "last_name": ""
}
```

'GET'
output:
```
{
    "pk": 2,
    "username": "nosipov",
    "email": "nosipov@gmail.com",
    "first_name": "",
    "last_name": ""
}
```
----------------------------------------------------------------------

POST
/login/

input:

username
email
password

output:

```
{
    "key": "af459e5f6193ac95193ed5e0c28746a5af5a861d"
}
```
----------------------------------------------------------------------

# REST API


GET POST
/posts/

'POST'
input:
title
content
'also post have `owner` but if you create it `owner`is current user '

output:
```
{
    "id": 24,
    "title": "Cum pariatur Id qui",
    "content": "Cum pariatur Fugit",
    "owner": 2,
    "created": "2019-04-04T14:55:34.415067Z"
}
```

'GET'
output:
```
[
    {
        "id": 23,
        "title": "Assumenda rem suscip",
        "content": "Magni consectetur s",
        "owner": 2,
        "created": "2019-04-04T14:55:25.227006Z"
    },
    
    ....
    
    {
        "id": 28,
        "title": "Architecto doloremqu",
        "content": "Assumenda vel accusa",
        "owner": 2,
        "created": "2019-04-04T15:04:40.622459Z"
    }
]
```
----------------------------------------------------------------------

GET PUT DELETE
/posts/<int>/

'PUT'
input:
title
content
owner

output:
```
{
    "id": 24,
    "title": "Cum pariatur Id qui",
    "content": "Cum pariatur Fugit",
    "owner": 2,
    "created": "2019-04-04T14:55:34.415067Z"
}
```

GET
output:
```
{
    "id": 24,
    "title": "Cum pariatur Id qui",
    "content": "Cum pariatur Fugit",
    "owner": 2,
    "created": "2019-04-04T14:55:34.415067Z"
}
```
----------------------------------------------------------------------

GET 
/users/

```
[
    {
        "id": 1,
        "username": "admin",
        "email": "admin@admin.pro",
        "first_name": "",
        "last_name": "",
        "subscribers": [
            2
        ]
    },
    
    ....
    
    {
        "id": 2,
        "username": "nosipov",
        "email": "nosipov@gmail.com",
        "first_name": "",
        "last_name": "",
        "subscribers": [
            3
        ]
    }
]
```

GET
/users/by_posts/

'same as above only sequence is ordered by count posts'
----------------------------------------------------------------------

GET
/users/<int>/

```
{
    "id": 1,
    "username": "admin",
    "email": "admin@admin.pro",
    "first_name": "",
    "last_name": "",
    "subscribers": [
        2
    ]
}
```

POST
/users/<int>/subscribe
/users/<ins>/unsubscribe

400 bad request
```
{
    "subscribers": "Any errort"
}
```

200 OK
`nothing`

----------------------------------------------------------------------











