from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('sign_up/', include('rest_auth.registration.urls')),
    path('', include('rest_auth.urls')),
    path('', include('service.urls')),
]
