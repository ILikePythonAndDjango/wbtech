from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import connection
from django.db import models


class User(AbstractUser):

    subscribers = models.ManyToManyField(
        'self', symmetrical=False,
        db_table='service_user_subscribers', blank=True)
    feeds = models.ManyToManyField(
        'Post', through='Feed', through_fields=('user', 'post'), blank=True)


class Post(models.Model):

    title = models.CharField(max_length=100)
    content = models.TextField()
    owner = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('created',)


@receiver(post_save, sender=Post)
def create_feed(sender, created=False, instance=None, **kwargs):
    """
    When the post was created you cannot use QuerySet API for creating
    `Feed`, because it's very costly.
    """
    if created and instance:
        with connection.cursor() as cursor:
            cursor.execute("""
                INSERT INTO service_feed (is_read, user_id, post_id)
                SELECT false, to_user_id, %s FROM service_user_subscribers
                WHERE from_user_id = %s
            """, (instance.id, instance.owner_id))


class Feed(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    is_read = models.BooleanField(default=False, blank=True)

    def to_read(self):
        if not self.is_read:
            self.is_read = True
            self.save()
        return self

    class Meta:
        db_table = 'service_feed'
        unique_together = ('user', 'post')
