from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from .models import Post, User


class FeedPostTests(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls._pass = 'qwerty1234'

        # Set users
        cls._bloger = User.objects.create_user(
            username='login1', email='login1@gmail.com', password=cls._pass,
            is_active=1, is_staff=1)
        cls._subscriber = User.objects.create_user(
            username='login2', email='login2@gmail.com', password=cls._pass,
            is_active=1, is_staff=1)
        cls._another = User.objects.create_user(
            username='login3', email='login3@gmail.com', password=cls._pass,
            is_active=1, is_staff=1)

    def test_subscribing(self):
        # login of subscriber
        self.client.login(
            username=self._subscriber.username, password=self._pass)

        url = reverse('user-subscribe', args=[self._bloger.id])
        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self._bloger.subscribers.count(), 1)

    def test_endpoint_for_creating_post(self):
        # login of bloger
        self.client.login(username=self._bloger.username, password=self._pass)

        url = reverse('post-list')
        response = self.client.post(
            url, {'title': 'title', 'content': 'content'})

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        post = Post.objects.first()
        self.assertEqual(Post.objects.count(), 1)
        self.assertEqual(post.title, 'title')
        self.assertEqual(post.owner_id, self._bloger.id)

    def test_subscribing_on_himself(self):
        self.client.login(username=self._another.username, password=self._pass)

        url = reverse('user-subscribe', args=[self._another.id])
        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(self._another.subscribers.count(), 0)
        self.assertDictEqual(
            response.data, {'subscribers': 'You are owner of this post'})

    def test_unsubscribing_on_himself(self):
        self.client.login(username=self._another.username, password=self._pass)

        url = reverse('user-unsubscribe', args=[self._another.id])
        response = self.client.post(url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.data, {'subscribers': 'You are not subscribed'})
