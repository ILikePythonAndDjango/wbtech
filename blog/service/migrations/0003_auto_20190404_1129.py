# Generated by Django 2.1.7 on 2019-04-04 11:29

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0002_auto_20190403_1847'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='subscribers',
            field=models.ManyToManyField(
                blank=True, to=settings.AUTH_USER_MODEL),
        ),
    ]
