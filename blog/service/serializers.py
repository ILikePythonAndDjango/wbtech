from rest_framework import serializers
from .models import User, Post, Feed


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'subscribers',
        )


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('id', 'title', 'content', 'owner', 'created')


class FeedSerializer(serializers.ModelSerializer):

    post = PostSerializer(read_only=True)

    class Meta:
        model = Feed
        fields = ('id', 'post', 'is_read')
