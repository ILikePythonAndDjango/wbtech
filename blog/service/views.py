from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets, status
from django.db.models import Count
from . import serializers
from .models import Post, Feed, User


class FeedPagination(PageNumberPagination):

    page_size = 10

    def get_paginated_response(self, data):
        return Response(data)


class FeedViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Feed.objects.all()
    pagination_class = FeedPagination
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.FeedSerializer
    filterset_fields = ('is_read',)

    def initialize_request(self, request, *args, **kwargs):
        self.queryset = Feed.objects.filter(user=request.user)
        return super().initialize_request(request, *args, **kwargs)

    def retrieve(self, *args, **kwargs):
        self.get_object().to_read()
        return super().retrieve(*args, **kwargs)


class UserViewSet(viewsets.ReadOnlyModelViewSet):

    """
    You cannot sign up via `/users/` endpoint.
    """

    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer

    @action(detail=False)
    def by_posts(self, request):
        users = User.objects.annotate(Count('post'))
        self.queryset = users.order_by('post__count')
        return self.list(request)

    @action(detail=True, methods=('post',))
    def subscribe(self, request, pk=None):
        owner = self.get_object()
        if owner.id == request.user.id:
            return Response(
                {'subscribers': 'You are owner of this post'},
                status=status.HTTP_400_BAD_REQUEST
            )
        owner.subscribers.add(request.user)
        return Response()

    @action(detail=True, methods=('post',))
    def unsubscribe(self, request, pk=None):
        owner = self.get_object()
        try:
            owner.subscribers.get(id=request.user.id)
            owner.subscribers.remove(request.user)
            return Response()
        except User.DoesNotExist:
            return Response(
                {'subscribers': 'You are not subscribed'},
                status=status.HTTP_400_BAD_REQUEST
            )


class PostViewSet(viewsets.ModelViewSet):

    queryset = Post.objects.all()
    serializer_class = serializers.PostSerializer

    def perform_create(self, serializer):
        """
        Saves post with current user as owner
        """
        serializer.save(owner=self.request.user)
